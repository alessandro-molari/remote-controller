const { setupApp, startApp } = require("./src/server");

const app = setupApp();
startApp(app);
