const fs = require("fs");
const { promisify } = require("util");

const request = require("supertest");
const expect = require("chai").expect;

const { setupApp } = require("../../src/server");
const { secret: secretConfig } = require("../../src/config");
const { encodeB64Url } = require("../../src/util/b64");

const readFile = promisify(fs.readFile);

const encodedSecret = encodeB64Url(secretConfig.value);

const inputMachineId = "obs-tec-wrk01";

describe("Power Management Controller", () => {
  const commands = {
    reboot: "systemctl reboot",
    poweroff: "systemctl poweroff"
  };

  for (const [name, command] of Object.entries(commands)) {
    const path = `/pm/${name}`;

    describe(path, () => {
      it(`Should trigger ${name} command`, async () => {
        const response = await request(setupApp())
          .post(path)
          .set("Authorization", `Bearer ${encodedSecret}`)
          .send({ machineId: inputMachineId })
          .expect(200);

        expect(response.body.stderr).to.be.empty;
        expect(response.body.stdout).to.be.eq(
          `Command (machine: ${inputMachineId}) => ${command}`
        );
      });
    });
  }
});
