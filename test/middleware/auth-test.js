const fs = require("fs");
const { promisify } = require("util");

const request = require("supertest");
const expect = require("chai").expect;

const { setupApp } = require("../../src/server");
const { secret: secretConfig, jwt: jwtConfig } = require("../../src/config");
const { encodeB64Url } = require("../../src/util/b64");
const { jwtVerify, jwtGenerateAuthToken } = require("../../src/util/jwt");
const { generateBearer, parseBearer } = require("../../src/util/bearer-token");

const readFile = promisify(fs.readFile);

const encodedSecret = encodeB64Url(secretConfig.value);

let jwtClientPK;
let jwtServerCert;

describe("Authentication Middleware", () => {
  before("Load external resources", async () => {
    jwtServerCert = await readFile(jwtConfig.server.certPath, {
      encoding: "utf8"
    });
    jwtClientPK = await readFile(jwtConfig.client.pkPath, { encoding: "utf8" });
  });

  it("Should authenticate with plain secret/JWT", async () => {
    const responsePlainSecret = await request(setupApp())
      .post("/pm/poweroff")
      .set("Authorization", `Bearer ${encodedSecret}`)
      .send({ machineId: "obs-tec-wrk01" })
      .expect(200);

    const serverToken = parseBearer(responsePlainSecret.headers.authorization);
    const token = await jwtVerify(serverToken, jwtServerCert);
    const clientToken = await jwtGenerateAuthToken(token.secret, jwtClientPK);

    const responseJWT = await request(setupApp())
      .post("/pm/poweroff")
      .set("Authorization", `Bearer ${clientToken}`)
      .send({ machineId: "obs-tec-wrk01" })
      .expect(200);
  });

  it("Should reject invalid machine identifier", async () => {
    try {
      await request(setupApp())
        .post("/pm/poweroff")
        .set("Authorization", `Bearer ${encodedSecret}`)
        .send({ machineId: "obs-ced-sct01" });
    } catch (error) {
      expect(error.response.statusCode).to.be.eq(400);
    }
  });
});
