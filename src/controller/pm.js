const { check } = require("express-validator/check");

const { controller: controllerConfig } = require("../config");
const { buildHandler } = require("../util/handler");
const { execSaltCommand, execDummyCommand } = require("../util/exec-command");
const { createError } = require("../util/error");
const { Merror } = require("express-merror");

async function execSingleCommand(machineId, command) {
  let result;
  try {
    switch (controllerConfig.pm.execStrategy) {
      case "saltstack":
        result = await execSaltCommand(machineId, command);
        break;
      case "dummy":
        result = await execDummyCommand(machineId, command);
        break;
      default:
        return createError(500, "Invalid execution strategy", {
          strategy: controllerConfig.pm.execStrategy
        });
    }
  } catch (error) {
    return createError(500, "Failed to run command", error.message);
  }
  return { stdout: result.stdout, stderr: result.stderr };
}

async function handleReboot(req, res, next) {
  const result = await execSingleCommand(
    req.body.machineId,
    "systemctl reboot"
  );
  if (result instanceof Merror) {
    next(result);
  } else {
    res.send(result);
  }
}

async function handlePoweroff(req, res, next) {
  const result = await execSingleCommand(
    req.body.machineId,
    "systemctl poweroff"
  );
  if (result instanceof Merror) {
    next(result);
  } else {
    res.send(result);
  }
}

module.exports.pmControllerInfo = [
  {
    path: "/pm/reboot",
    validations: [
      check("machineId").matches(controllerConfig.pm.machineIdRegexp)
    ],
    handler: buildHandler(handleReboot)
  },
  {
    path: "/pm/poweroff",
    validations: [
      check("machineId").matches(controllerConfig.pm.machineIdRegexp)
    ],
    handler: buildHandler(handlePoweroff)
  }
];
