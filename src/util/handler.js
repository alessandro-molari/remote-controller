const { Merror } = require("express-merror");
const { validationResult } = require("express-validator/check");

function buildHandler(handler) {
  return async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return next(new Merror(400, "Invalid input parameters.", errors.array()));
    }
    try {
      return await handler(req, res, next);
    } catch (error) {
      return next(new Merror(500, error.message));
    }
  };
}

module.exports.buildHandler = buildHandler;
